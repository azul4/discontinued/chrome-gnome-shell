# chrome-gnome-shell

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

Native browser connector for integration with extensions.gnome.org

https://wiki.gnome.org/Projects/GnomeShellIntegrationForChrome

https://gitlab.gnome.org/GNOME/chrome-gnome-shell/

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/gnome/chrome-gnome-shell.git
```
